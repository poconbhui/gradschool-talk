include Latex.make


all: $(SUBDIRS) latex


# Add Latex Targets Macro
define ADD_LATEX
TARGET_NAME=$(1)
#TARGET_SOURCES=$$($$(TARGET_NAME)_SOURCES)
#TARGET_DEPS=$$($$(TARGET_NAME)_DEPS)

$$(TARGET_NAME): $$($$(TARGET_NAME)_SOURCES)
	echo $$(TARGET_NAME), $$($$(TARGET_NAME)_SOURCES), $$($$(TARGET_NAME)_DEPS)
	$$(PDFLATEX) $$(LATEX_FLAGS) $$($$(TARGET_NAME)_SOURCES)

# Add to target latex
latex: $$(TARGET_NAME)

endef


# Add targets for each entry in latex_outputs
$(foreach latex_output, $(latex_outputs),          \
	$(eval $(call ADD_LATEX, $(latex_output))) \
)


# Make subdirs
.PHONY: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@

# Clean subdirs and top dir
clean:
	$(foreach DIR, $(SUBDIRS), $(MAKE) -C $(DIR) clean)
	rm -f *.pdf *.aux *.log *.nav *.out *.snm *.toc
