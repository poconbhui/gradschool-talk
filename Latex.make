PDFLATEX=pdflatex
LATEX_FLAGS= -halt-on-error -file-line-error


latex_outputs = gradschool_talk.pdf


gradschool_talk.pdf_SOURCES = gradschool_talk.tex
gradschool_talk.pdf_DEPS = headers.tex copywriting.tex


SUBDIRS=images/domain_states
