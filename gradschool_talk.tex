\documentclass{beamer}


\author[Padraig O Conbhui]{P\'adraig \'O Conbhu\'\i}
\title{Micromagnetism and Magnetostriction}
\subtitle{A computational field guide}
\institute[UoE]{%
    School of GeoSciences \\
    University of Edinburgh
}
\date{March 5th, 2014}


\input{headers}

\begin{document}



\begin{frame}[plain]
    \titlepage
\end{frame}

\section{Micromagnetism}

    \subsection{Background}
        \begin{frame}
            \frametitle{Background}

            \begin{columns}
                \column{0.49\textwidth}
                    \structure{Micromagnetism:}
                    \begin{itemize}
                        \item Numerical model of domain structures.
                        \item Continuum approximation.
                    \end{itemize}

                    \begin{center}
                        \includegraphics[width=\textwidth]{%
                            images/large_cube_vortex_state.pdf%
                        }
                    \end{center}


                \column{0.49\textwidth}
                    \structure{Motivation:}
                    \begin{itemize}
                        \item
                            Magnetic field in rocks a result of a large number
                            of small magnetic crystals.

                        \item
                            Magnetic (domain) configuration and stability
                            for many crystals cannot
                            be worked out analytically.

                        \item
                            Configuration/stability useful for
                            paleomagnetism,
                            archeomagnetism,
                            magnetostratigraphy,
                            plate tectonics.
                    \end{itemize}
            \end{columns}
        \end{frame}

    \subsection{Computational Approach}

        \begin{frame}
            \frametitle{Energy Calculation}

            \begin{columns}[t]
                \column{0.49\textwidth}
                    \structure{Energy and H-field:}
                    \begin{align*}
                        E           &= E(M, H)
                        \\
                        H_{eff}     &= \pdiff{E}{M}
                        \\
                        \diff{M}{t} &= \text{LLG}(M, H)
                    \end{align*}

                \column{0.49\textwidth}
                    \structure{Discrete solution:}
                    \begin{align*}
                        E_i     &= E(M_i, H_i)
                        \\
                        H_{i+1} &= \pdiff{E_i}{M}
                        \\
                        M_{i+1} &= \text{step}(M_i, H_i)
                    \end{align*}
            \end{columns}

            \bigskip

            \structure{Stepping functions:}
            \begin{itemize}
                \item Optimiser (eg CG energy optimiser)
                \item Dynamic time step (LLG) + integrator
            \end{itemize}
        \end{frame}


    \subsection{Existing Work}

        \begin{frame}
            \frametitle{Existing Micromagnetic Models}

            \begin{columns}

                \column{0.75\textwidth}
                    \structure{Finite Element Models}
                    \begin{itemize}
                        \item Long standing Fortran code by Wyn Williams.
                        \item FEniCS MicroMag package by Lesleis Nagy.
                        \item Azoum and Besbes magnetostrictive model.
                    \end{itemize}

                    \bigskip

                    \structure{Finite Difference Models}
                    \begin{itemize}
                        \item Magnetostrictive model by Karl Fabian.
                    \end{itemize}

                \column{0.35\textwidth}
                    \begin{center}
                        \includegraphics[width=0.8\textwidth]{%
                            images/cube_flower_state.pdf%
                        }

                        \bigskip

                        \includegraphics[width=0.8\textwidth]{%
                            images/cube_vortex_state.pdf%
                        }
                    \end{center}
                        
            \end{columns}
        \end{frame}


        \begin{frame}
            \frametitle{Phenomena Considered}

            \begin{columns}

                \column{0.49\textwidth}
                    \structure{State of the Art:}
                    \begin{itemize}
                        \item Anisotropy Energy
                        \item Exchange Energy
                        \item Magnetostatic Energy
                    \end{itemize}

                    \bigskip

                    \alert{Ignored:}
                    \begin{itemize}
                        \item Magnetostriction
                    \end{itemize}

                \column{0.49\textwidth}
                    \includegraphics[width=\textwidth]{%
                        images/anisotropy_energy_surface.pdf%
                    }

            \end{columns}

        \end{frame}


\section{Magnetostriction}

    \subsection{Background}
        \begin{frame}
            \frametitle{Emergence of Magnetostriction}

            \structure{Anisotropy Energy $\leftrightarrow{}$ Magnetostriction}
            \begin{itemize}
                \item
                    Anisotropy Energy a result of spatial position of
                    electrons and spin direction.
                    \\
                    $E_A = E_A(x_i, \dots{}, x_n, s_i, \dots{}, s_n)$

                \item
                    The Anisotropy Energy distribution can be changed by
                    changing the spatial position of the electrons.

                \item
                    Change in unit cell shape $\leftrightarrow{}$ change in
                    Anisotropy Energy distribution.
            \end{itemize}

            \bigskip

            \structure{Result:}
            Energy can be further minimised by balancing reduction in
            anisotropy energy with gain in elastic energy.

        \end{frame}


        \begin{frame}
            \frametitle{Importance of Magnetostriction}

            \begin{itemize}
                \item
                    Critical for correct prediction of domain configurations.
                    \\
                    cf. Kittel 1949

                \item
                    In some systems,
                    \begin{math}
                        \bigO{\text{Elastic Energy}}
                            \sim{} \bigO{\text{Anisotropy Energy}}
                    \end{math}
            \end{itemize}


            \begin{center}
                \includegraphics{%
                    images/kittel_fig33_magnetostriction_on_domains.pdf%
                }
            \end{center}
        \end{frame}


    \subsection{Implementation}

        \begin{frame}
            \frametitle{Difficulty with Magnetostriction}

            \begin{columns}[t]
                \column{0.49\textwidth}
                    \structure{Normally:}
                    \begin{align*}
                        E    &= E(M, H)
                        \\
                        H(M) &= \pdiff{E}{M}
                        \\
                        \diff{M}{t} &= \text{LLG}(M, H)
                    \end{align*}

                \column{0.49\textwidth}
                    \structure{With Magnetostriction:}
                    \begin{align*}
                        E                       &= E(M, H, \epsilon{}, \sigma{})
                        \\
                        H(M, \epsilon{})        &= \pdiff{E}{M}
                        \\
                        \sigma{}(M, \epsilon{}) &= \pdiff{E}{\epsilon{}}
                        \\
                        \diff{M}{t} &= \text{LLG}(M, H)
                        \\
                        \diff{\epsilon}{t} &= \text{Hooke}(\epsilon, \sigma)
                    \end{align*}
            \end{columns}
        \end{frame}


        \begin{frame}
            \frametitle{Difficulty with Implementation}

            \structure{Normally:}
                Solve for $\phi$.

            \bigskip

            \structure{With Magnetostriction:}
                Simultaneous solve for $\phi$ and $\vec{u}$ with
                coupling.

            \bigskip
            \bigskip

            \structure{Note:}
            \begin{itemize}
                \item
                    $\phi$ is the magnetic scalar potential.
                    \\
                    It's used to derive magnetic fields etc.
                    in permanent magnets.

                \item
                    $\vec{u}$ is the displacement field.
                    \\
                    It's used to derive stress/strain in
                    elastic materials.
            \end{itemize}

        \end{frame}


    \subsection{Existing Work}

        \begin{frame}
            \frametitle{Existing Work}

            \structure{Karl Fabian}
            \begin{itemize}
                \item FDM with continuum defects approach.

                \item Looks discontinuous by design.

                \item
                    Work needed to introduce different boundary conditions
                    to this model.

                \item
                    Current implementation overestimates expected
                    values for Magnetite by order of 2-4.
            \end{itemize}

            \structure{Azoum and Besbes}
            \begin{itemize}
                \item FEM model with thermo-like approach.

                \item
                    Solving for extra displacement vector coupled with
                    solve for magnetic vector potential.

                \item Solvable with FEniCS.

                \item With some work, should be pluggable into Les's code.

                \item No rock magnetic results to date.
            \end{itemize}

        \end{frame}


\section{Expected Results}

    \begin{frame}
        \frametitle{Expected Results}

        \begin{itemize}
            \item A FEM code including magnetostrictive effects.

            \item
                Predictions of configurations and stabilities of
                magnetic crystals in various situations.

                \begin{itemize}
                    \item
                        Transition boundaries between domain configurations.
                        (SP, SD, PSD, MD)

                    \item
                        Oxidation effects.
                        Eg Core shell model of
                        maghemite and magnetite. Cracking a clear
                        indicator of large stresses in the system.
                \end{itemize}

            \item Fame and glory.

        \end{itemize}
    \end{frame}


    \begin{frame}[plain]
        \begin{center}
            \structure{\huge Thank You!}
            
            \bigskip

            \huge Questions?
            
            \smallskip

            \small (Nothing too hard, please.)
        \end{center}
    \end{frame}

\end{document}
