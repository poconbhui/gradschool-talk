#!/usr/bin/env gnuplot

set term pdf
set output "single_domain.pdf"

load "palette.plt"

set cbrange [0:100]

unset border
unset tics
unset key
unset title
unset colorbox

set view 14,55


splot '<./gen_single_domain.sh -2 2 -2 2 -2 2' using 1:2:3:4:5:6:7 with vectors lt palette
