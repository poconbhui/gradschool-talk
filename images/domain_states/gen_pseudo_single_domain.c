#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
    float x;
    float y;
} vec;


float length(vec v) {
    return sqrt(v.x*v.x + v.y*v.y);
}

vec norm(vec v) {
    float l = length(v);

    v.x = v.x/l;
    v.y = v.y/l;

    return v;
}

vec get_vec(float x, float y) {
    vec v = {-y, x};

    return norm(v);
}

float get_curl(float x, float y) {
    vec v = {x, y};
    float l = length(v);

    return (x*x + y*y)/(l*l*l);
    
}

int main(int argc, char* argv[]) {
    int x_min;
    int x_max;
    int y_min;
    int y_max;
    int z_min;
    int z_max;

    if(argc != 7) {
        printf("Expecting 4 arguments\n");
        printf("Usage: %s x_min x_max y_min y_max z_min z_max\n", argv[0]);

        return EXIT_FAILURE;
    }

    x_min = atoi(argv[1]);
    x_max = atoi(argv[2]);
    y_min = atoi(argv[3]);
    y_max = atoi(argv[4]);
    z_min = atoi(argv[5]);
    z_max = atoi(argv[6]);

    for(int z=z_min; z<z_max; ++z) {
        for(int x=x_min; x<x_max; ++x)
        for(int y=y_min; y<y_max; ++y) {
            if(x == 0 && y == 0) continue;

            vec   v = get_vec(x, y);
            float c = get_curl(x, y);

            printf("%d %d %d %f %f %f %f\n", x, y, z, v.x, v.y , 0.0, 100*c);
        }

        printf("\n");
    }

    return EXIT_SUCCESS;
}
