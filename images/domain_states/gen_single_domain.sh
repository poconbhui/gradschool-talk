#!/usr/bin/env bash

x_min=$1
x_max=$2
y_min=$3
y_max=$4
z_min=$5
z_max=$6


for z in $(seq $z_min $z_max); do
    for x in $(seq $x_min $x_max); do
        for y in $(seq $y_min $y_max); do
            echo $x $y $z 0.5 0 0 0
        done
    done

    echo
done
