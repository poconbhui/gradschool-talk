#!/usr/bin/env gnuplot

set term pdf
set output "pseudo_single_domain.pdf"

load "palette.plt"


set cbrange [10:70]

unset border
unset tics
unset key
unset title
unset colorbox

set view 14,55
splot '<./gen_pseudo_single_domain.x -5 5 -5 5 -2 2' using 1:2:3:4:5:6:7 lc palette with vectors
