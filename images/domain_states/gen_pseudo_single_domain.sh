#!/usr/bin/env bash

x_min=$1
x_max=$2
y_min=$3
y_max=$4


for x in $(seq $x_min $x_max); do
    for y in $(seq $y_min $y_max); do
        length=$( echo "sqrt( ($x) * ($x) + ($y) * ($y) )" | bc -l)
        if [ $length = "0" ]; then length=1; fi
        x_hat=$(echo "$x / $length" | bc -l)
        y_hat=$(echo "- $y / $length" | bc -l )

        curl=$(echo "(($x_hat)*($x_hat)/$length - ($y_hat)*($y_hat)/$length)" | bc -l)

        echo $x $y $y_hat $x_hat $curl
    done
done
